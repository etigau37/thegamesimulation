# Author: Etienne Gauthier
# ------------------------------
import os
import sys
import random
import argparse

# ------------------------------
parser = argparse.ArgumentParser(prog='main.py')
parser.add_argument('-np', type=int, required=True, help='number of players')
# parser.add_argument('-A', action='store_true', help='Tous les auteurs')
# parser.add_argument('-f', help='Fichier inconnu a comparer')
# parser.add_argument('-m', required=True, type=int, choices=range(1,3), help='Mode (1 ou 2) - unigrammes ou digrammes')
# parser.add_argument('-G', type=int, help='Taille du texte a generer')
args = parser.parse_args()
# ------------------------------

'''
ToDo:
    - nerfer choose starting bot (mettre des paliers tel que commenté)
    - Allow bots to "block" a base (don't play on this base)
	- Choose a unique word between bot and player to avoid confusion
	- Add unit tests?
	- Be consistent with the use of "self"
	- Switch case that takes number of Bots into account when distributing cards
	- Fix the colored input that doesn't work in terminal
'''

# ------------------------------
class Base:

    def __init__(self, direction, id):
        self.direction = direction
        self.id = id
        if direction == "upward":
            self.playedCards = [1]
        elif direction == "downward":
            self.playedCards = [100]
        else:
            print("Invalid base direction!")
        
    def current(self):
        return self.playedCards[-1]

# ------------------------------
class BestBaseForCard:

    def __init__(self, card, bestBaseId, bestJump):
        self.card = card
        self.bestBaseId = bestBaseId
        self.bestJump = bestJump

# ------------------------------
class Bot:

    def __init__(self, id):
        self.id = id
        self.cards = []

    # Returns the best possible base to play on for each card
    def evaluateCards(self, bases):
        bestBaseToPlayOnForEachCard = []
        for card in self.cards:
            bestJump = 1000
            bestBase = None
            for base in bases:
                    if base.direction == "upward":
                        if card - base.current() > 0:
                            jump = card - base.current()
                            if jump < bestJump:
                                bestJump = jump
                                bestBase = base.id
                        #looking for a 10-hop backwards
                        if card - base.current() == -10:
                            bestJump = -10
                            bestBase = base.id
                            print('\033[94m' + "this bot will play a 10-hop backwards!!" + '\033[0m')
                    if base.direction == "downward":
                        if base.current() - card > 0:
                            jump = base.current() - card
                            if jump < bestJump:
                                bestJump = jump
                                bestBase = base.id
                        # looking for a 10-hop upwards
                        if base.current() - card == -10:
                            bestJump = -10
                            bestBase = base.id
                            print('\033[94m' + "this bot will play a 10-hop upwards!!" + '\033[0m')  
            bestBaseToPlayOnForEachCard.append(BestBaseForCard(card, bestBase, bestJump))
        bestBaseToPlayOnForEachCard.sort(key=lambda x: x.bestJump)
        return bestBaseToPlayOnForEachCard

    def playCard(self, bestBaseToPlayOnForEachCard, bases):
        if bestBaseToPlayOnForEachCard[0].bestJump == 1000:
            print('\033[91m' + "GAME OVER, bot #" + str(self.id) + " can't play anymore")
            print("bot #" + str(self.id) + " cards: ")
            print("\t", self.cards)
            sys.exit(0)

        # Play the card which has the tiniest jump on the appropriate base
        for base in bases:
            if base.id == bestBaseToPlayOnForEachCard[0].bestBaseId:
                base.playedCards.append(bestBaseToPlayOnForEachCard[0].card)
                self.cards.remove(bestBaseToPlayOnForEachCard[0].card)
                break

    # This method is called within the Simulator
    def drawCard(self, deck):
        self.cards.append(deck[-1])
        deck.pop()

# ------------------------------
class Simulator:
    numberOfPlayers = 0
    players = []
    deck = []
    bases = []
    bases.append(Base("upward", "u1"))
    bases.append(Base("upward", "u2"))
    bases.append(Base("downward", "d1"))
    bases.append(Base("downward", "d2"))

    def __init__(self, numberOfPlayers):
        self.numberOfPlayers = numberOfPlayers
        # Deck initialization
        for i in range(2, 100):
            self.deck.append(i)
            random.shuffle(self.deck)
        # Player initialization
        for i in range(numberOfPlayers):
            self.players.append(Bot(i+1))

    def distributeCards(self):
        if self.numberOfPlayers == 4:
            for player in self.players:
                hand = random.sample(self.deck, 6)
                for card in hand:
                    player.cards.append(card)
                    self.deck.remove(card)
                hand.clear()

    def startSimulation(self):
        startingBot = self.chooseStartingBot()
        botToPlay = startingBot[0]
        turnNumber = 1
        while True:
            botToPlay = (botToPlay % (args.np+1))
            # Verify if all players have an empty hand
            if self.verifyVictory():
                self.printVictory()
                sys.exit(0)
            print("Turn #" + str(turnNumber) + ", Bot #" + str(self.players[botToPlay-1].id) + " playing...")
            self.printEachBot()
            # Add here mechanic to block a base

            # Playing first card
            # Looking if bot has empty hand
            if not self.verifyBotHand(self.players[botToPlay-1]):
                bestBaseToPlayOnForEachCard = self.players[botToPlay-1].evaluateCards(self.bases)
                self.players[botToPlay-1].playCard(bestBaseToPlayOnForEachCard, self.bases)
                bestBaseToPlayOnForEachCard.clear()
                # The player must play a second card as long as the deck is not empty
                if len(self.deck) != 0 and not self.verifyBotHand(self.players[botToPlay-1]):
                    # Playing second card
                    bestBaseToPlayOnForEachCard = self.players[botToPlay-1].evaluateCards(self.bases)
                    self.players[botToPlay-1].playCard(bestBaseToPlayOnForEachCard, self.bases)
                    bestBaseToPlayOnForEachCard.clear()
                    # Bot will play as long as he has 3-hop or -10-hop cards, and has long that he has cards (of course)
                    bestBaseToPlayOnForEachCard = self.players[botToPlay - 1].evaluateCards(self.bases)
                    while (not self.verifyBotHand(self.players[botToPlay-1])) and (bestBaseToPlayOnForEachCard[0].bestJump <= 3):
                            self.players[botToPlay-1].playCard(bestBaseToPlayOnForEachCard, self.bases)
                            bestBaseToPlayOnForEachCard.clear()
                            bestBaseToPlayOnForEachCard = self.players[botToPlay - 1].evaluateCards(self.bases)
                    while len(self.deck) != 0 and len(self.players[botToPlay-1].cards) != 6:
                        self.players[botToPlay-1].drawCard(self.deck)
            self.printBases()
            botToPlay += 1
            if botToPlay == args.np + 1:
                botToPlay = 1
            turnNumber += 1

    def chooseStartingBot(self):
        # bon total de 10 == je peux commmencer
        # bon total de 15 == je peux commencer si personne peut
        # bon total de 20 == ouin bof si VRM personne peut je peux y aller
        # bon supérieur à 20, je veux vrm pas commencer, au hasard si tout le monde est comme ça

        startingTotalHop = [1,100,200,200]  # botID, firstCard, secondCard, totalHop
        for bot in self.players:
            bot.cards.sort()
            if bot.cards[1] < startingTotalHop[3]:
                startingTotalHop.clear()
                startingTotalHop = [bot.id, bot.cards[0], bot.cards[1], bot.cards[1]]
            if 100 - bot.cards[-2] < startingTotalHop[3]:
                startingTotalHop.clear()
                startingTotalHop = [bot.id, bot.cards[-1], bot.cards[-2], 100 - bot.cards[-2]]

        return startingTotalHop

    def verifyVictory(self):
        victory = False
        victoryCounter = 0
        for bot in self.players:
            if len(bot.cards) != 0:
                break
            victoryCounter += victoryCounter
        if victoryCounter == self.numberOfPlayers:
            victory = True
        return victory

    def verifyBotHand(self, bot):
        emptyHand = False
        if len(bot.cards) == 0:
            emptyHand = True
        return emptyHand

    def printEachBot(self):
        print("\n***************************")
        for player in self.players:
            print("Bot #" + str(player.id) + ": ")
            print("\t", player.cards)
        print("***************************")

    def printBases(self):
        print("***************************")
        for base in self.bases:
            print(base.id + ": ")
            print(base.playedCards)
        print("***************************")

    def printVictory(self):
        print("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
        print("-*-*-*-*-*-*-VICTORY-*-*-*-*-*-*")
        print("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")




#  ------------------------------
if __name__ == "__main__":
    simulator = Simulator(args.np)
    simulator.distributeCards()
    simulator.startSimulation()

#  ------------------------------


