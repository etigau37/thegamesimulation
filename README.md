# TheGameSimulation
***************************************************************
Simulation for the board game "The Game" by Steffen Benndorf
***************************************************************
The goal of this simulation is to see whether it's possible to beat
the game every time, for any starting configuration and number of players.
As humans, we are not perfect and we do not always play the best card,
e.g players sometimes don't notice that they can play a 10-hop card in the other direction.
With Bots that evaluate every possible move/stratgey, I hope to find the strategy to achieve
the 100% win rate everytime.
***************************************************************
# Official Rules
***************************************************************
See pdf within repo